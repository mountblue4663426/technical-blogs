<br/>

Kid: (Yells Loudly) Hey! computer, what is 63 multiplied 98.<br/>
Computer:   *_no-response_ <br/>
Kid:  and I thought computers were crazy fast in calculations...

<br/>


# CLI

## **What are we going to learn?**
1. [Overview: How do software and hardware communicate.](#1-overview-how-do-software-and-hardware-communicate)
    * Hardware
    * Kernel
    * Shell

2. [CLI](#2-cli)
    * Why CLI?
    * Commands

3. [CLI commands](#3-cli-commands)
    * Files and Folders
    * OS & Processes
    * Network

<br/>

## 1. **Overview: How do software and hardware communicate**
The world of computers deal with zeros and ones, present everywhere around us from deep sea submarines to satellites in space, carrying out taks assigned to them.But there arises a question, who is telling those machines to do the work they are doing. Though the answer can be simple i.e humans, but how ? <br/><br/>

Now most of the modern day computer are composed of :
* Hardware
* Software

### Hardware
These are the components used in computer and responsible to carrying out the **_real_** work that has a definate outcome. Some examples of hardware components are:
  * CPU 
  * Hard-disk
  * RAM
  * Motherboard ...etc

These work with each other to carry out **assigned work**. But who assignes them work?

### Software
Software is responsible for assigning work to the hardware and ensure it's completion. As discussed earlier that, computers can only understand zeros and ones, it is the responsiblity of software to convert the human (user) input into hardware compatible/understandable language.<br/>


> Software is collection of programmed code, which gives set of instructions to the computer.

Sofware itself is composed of many interdependent constituents, such as **Kernal** and **Shell**

#### Kernal
It is the core of any Operating System (Software) that directly interacts with the hardware. It recieves the instructions from **shell**, and passes it to hardware which carries the work. Thus manages the effective communication between shell(user input) and hardware.

![Hardware-kernal-shell](./hardware-kernel-shell.png)


#### Shell
This is a program (software) which provides an interface for users to give input through keyboards (or other device). From the above image, we can observe that OS, consiting of kernal and shell(also other software) interact with utilities for various functions. <br/>
There are differnt shells like CSH( C Shell), KSH (Korne Shell) and one of the most popular is BASH (Bourne Again SHell). A user can acces SHELL in mainly the follwing ways:<br/>
* Terminal
* SSH

## 2. CLI
These SHELL provides an interface for the users to enter their commands through keyboard. This form of interface is called `Command Line Interface (CLI) `.
Here the users enter their commands which are taken by shell and passes to kernal , which finally passes to hardware for execution.<br/>

> CLI is simply a text based interface to interact with shell (computers)

### Why CLI?
In modern day computers, we are greated with great simple colorful UI (User Interface) in applications to interact with computers. So the question arises, why do we need CLI? or Why the engineerss created such form of interface?
*  Technological limitations: The initial stages of software development was mostly text-based, that was motly done through punch cards and keyboards. A complete interactive user interface was challenging. (which evolved over time)
*  Remote access: As CLI gives text-based interface it require very less resources or data transfers to interact with a remote servers. Thus provide secure and efficient remote system communication.
  
### Commands
As, discussed above CLI is text-based interface which takes user input, but does it understand common english, hindi or any other human languages. The answer is NO!. There are certain specific keywords that are used to invoke a desired behaviour, these are called ` commands `.These commands come with different _options_ and _flags_ which provides `varaitions`
in the very same command's behaviour.


## 3. CLI commands
There are lot of (ALOT!) of commands which a user can consider to use according to the need. The first command that is very crucial in learning other commands is 
```
man <command_name>
```
This provides detailed documentation of the specified command. This is useful while we are unsure about different options or it's usage and provides `system reference manuals.`

### File and Folders
Files acts as large containers storing information/data. There are different kinds of files like video files, image files, plain text files and so on.<br/>
Whereas Folders acts as containers for files and other sub-folders. In simple terms these are like cabinets storing different content.<br>
#### Basic CLI commands for files and folders
##### touch
```
 touch  <path_to_file/filename>
```
   * Used to update the access and modification time fo a file
   * if the file is not present in the **valid** directory it creates a file with passed name.

##### mkdir
```
mkdir <directory_path>
```
* Creates a directory if **valid**
* ` -p ` , creates parent if absent

##### cp 
```
cp <path_to_file/filename> <path_to_copy/>
```
* copes the specified file in the given directory
* ` -r ` , to recursively copy a direcotry
  
##### mv
```
mv <path/filename> <move_path/>
```
* moves the specified file/directory to the passed path

##### rm
```
rm <path/filename>
```
* delete the mentioned valid file
* ` -r `, to delete recursively in a directory.
  

### OS and processes
` Process `: A series of actions that are meant to complete a assigned task Simply put, it refers to a task getting executed. Operating system starts many processes without user input. Each process has a unique processid, an ppid (Parent Process Id) i.e the id of parent process and other information related to user, group...etc
#### Basic CLI commands for OS and process

##### ps
```
 ps      
```          
* It reports a snapshot of current processes running on the computer.
  * ` -A  ` shows **all** the process
  * ` -o  `, format the output as per our need.
  * ` --sort=VALUE ` , sort according to cpu `%cpu`, memory ` %mem `, 
  
##### top
```
  top                   
```
  * shows the current running process dynamically
##### pgrep
```
  pgrep "regex"
```
  * to find the pid of a process whose name matches the regex passed.
  * ` -i `, ignores the case  descirbed in regex

### Network
With evolving technologies, inernet connectivity has become inseperable part of our lives. We work on computers and these computers can communicate over differnt kinds of network with one another. There are some many ways to track, identify these networks and their activities.

#### Basic CLI commands for networks

##### ip
```
ip addr
```
* show our computers ip address
* ` -4 ` , shows ipv4 address
* ` -6 ` , show ipv6 address
  
##### nestat
```
netstat
```
* mainly used for printing network connections
*  ` -a `, to list all the connections on our computer.
  
##### dig
```
dig <url>
```
* a simple DNS lookup utility. (DNS in simple terms, store key-value pair of website name and ip address)
* ` -4 `, only ipv4 should be used
* ` -6 `, only ipv6 should be used
  
##### ping
```
ping <url> 
```
* sends a madatory network request for the server to respond and shows the statistics like avg. response time, packet transmission ...etc
* ` -c VALUE `, pings the website VALUE times and stops by showing the stats.

<br/>
<hr/>

Kid: Gives input through the CLI<br/>
<!-- CLI -> SHELL -> KERNEL -> HARDWARE
                            |
CLI <- SHELL <- KERNAL  <- PROCESSED -->
```
echo $((63 * 98))
``` 
Computer output: 
```
6174
```
Kid: WOAH!, computer really has blazing calculation speed !!
