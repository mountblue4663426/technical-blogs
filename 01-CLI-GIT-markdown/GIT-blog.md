# Git 

## **What are we going to learn?**
1. [Version control System (VCS)](#1-version-control-system-vcs)
    - Uses
    - Summarized history.
2. [Git](#2-git)
   - A great VCS
   - Setup
3. [Basics of Git](#3-basics-of-git)
   - Virtual zones and file movement
   - Getting started
     - add
     - commit
   - remote 
   - clone, fetch, pull, push
4. [Intermediate Git Concepts](#4-intermediate-git-concepts)
   - Head pointer
   - Branch
     - Uses
     - creation and working
   - Merging
     - merge
     - rebase
     - conflict
  


## 1. Version control System (VCS)
Let's imagine we are working on a project and you encounter a problem. We being problems solvers come up with multiple solutions.We simply make copies of the project and **modify** by implementing our ideas and test them seperately.These different copies are called**versions**.<br/>


> **Version** : A modified form of a previous project/something.
  
After testing the different versions, we may want:<br/>

* identify changes  in files
* finalize a sepcific version and continue working on it.
* combine features of different versions
* go back to previous original version.
* try different combinations of versions ...etc

This is where **Version Control System**  comes in, and provide features to work on different versions of our project.


>**Version Control System** (VCS): Software tool to manage different versions of our project [_files/source-code_].

### Uses
VCS are generally used in projects  for the following reasons:<br/>
* Tracking changes 
* Collaboration with others
* Parallel development. (Branches)
  
### Summarized History
Around 1990s, to fulfill the need of VCS, centralized version control system were being developed by many companies and collaborating developers. Some of the popular one were Subversion and perforce. 
#### Centralized Version Control System (CVCS)
Here, the entire project and its versions are stored on a central server. Developers can work on different versions while **connected** to the server. Thought it served the basic needs of developers, but had many drawbacks:<br/>
* Mandatory connection to central server
  * Developers/users always have to be connected to the server for synchroized development
* Risk of **single** server
  * If anything happens to the server, entire workflow wil be effected, in extreme cases the entire data can become corrupt.
  
To address the issues of CVCS, developers and companies worked on creating a Distributed Version Control System(DVCS). Some of the popular ones are **Git** and Bitkeeper.
#### Distributed Version Control System (DVCS)
* Here the **entire** project copies are available to every developer to work on. 
  * This gave flexibility to developers to work on different versions without mandatory connection to the server.
  * As developers have local copies on their computers, the acces and modification time is insignificant.
* In case of irreversible mishaps at server, entire project files can be obtained from any user and can be setup in no time.


## 2. Git
Now, we have some idea of DVCS and its features and how it came into existence and the reasons behind it.Git is one of the most popular DVCS in the market.<br/>

> **Git is an open-source Distributed Version Control System** 

### A Great VCS
There are many reasons that make git on the most popular DVCS, some of them are:
* open source, making the entire source code is available for everyone to check and contribute.
* Integrity, each commit has its own unique hash value calculated according the files at the time of commit.(we will learn more about commit)
* movement of files in during development, i.e different virtual zone and optimized file movement in them.
* a ton of helping documentation for reference
  

### Setup
To install git in any linux distro we use the package manager of that OS and use relevent command to install git.<br/>

Example:
* for Debian based Ubuntu<br/>
  ```
  sudo apt install git
  ```
* for fedora
  ```
  sudo dnf install git
  ```

Then to initialize a git in a directory
```
git init
```


## 3. Basics of Git
Here we will learn some simple concepts of git that includes virutal zones, basic commands like add, commit and push. Then we will get some idea about remote, clone, fetch and pull.

### Virtual Zones and file movement
As we move ahead with project development, we create and modify a lot of files. To track all these changes git has virutal zones to ease the tracking and managing them it makes movement of files in these virtual zones.<br/><br/>
There are three virtual zones
1. Working area
   * the directory in which we are working, from where we can track our file with help of git.
2. Staged area
   * after working on files and completing a set of work, we add to the staged area which are ready for commits (ready to save the current progress)
3. Commit Area
   * if we are satisfied with the current progress and save the entire progress in a unique and identifiable form, the files are moved to commit area.

### Getting Started
The file movement among these virutal zones have to explicitly guided through git commands, which are add, commit
* add 
  * to move files from working area to staged area
  ```
  git add <filename>
  ```
  * to add every file
  ```
  fit add .
  ```
* commit
  * to move files from  staged area to commit area, here it is mandatory to give a commit message through ` -m  `
  ```
  git commit -m "message
  ```

### remote
The english word remote means "_something that is away or distant from us_". Similarly, a remote in git refers to the a repsoitory i.e project's source code , away from our local machine. This remote repository can be stored on  a website, company server or anywhere on the internet.Some of the popular cloud hosting services are given by **Github**  and **Gitlab**.<r/>


> Git vs Github <br/>
> git is a Version Control System, whereas Github is service provider to store our repositories.

#### Add remote repositories
* It is achieved by using git command `remote add` <br/>
  ```
  git remote add <remote_name> <url>
  ```
  The remote name can be customized according to the developer.


  > The default remote name assigned by git is ` origin `


### clone, fetch, pull
To work on a remote repositories, there are multiple way to interact with the remote.


> Some of the below terms are explained in simple terms, and would require further understanding.

#### clone
To **copy** the entire project from a remote repo to our local machine, we use clone.
```
git clone <url>
```
### fetch
After working on our project, if we want our local repository to be updated as per the remote repository we use `fetch` , this updates the local repository according to updates on remote repositories. In simple words, it updates our local repository from the remote repository
```
git fetch <remote_name>
```

#### pull
This is a complex topic in its own, but it is similar to ` fetch ` with extra operations on the fetched files. For now we can understand it as, updating local repository exactly as remote repository.
```
git pull <remote_name>
```

#### push
As the word means, it is used to ` push ` i.e send our updated commits to a remote repository. This is done to keep the remote repository updated for futher development.
```
git push <remote_name>
```

## 4. Intermediate Git Concepts
This covers some intermdiate concepts like Head pointer,branches, their uses, merging and their types.

### Head pointer
A pointer is object which shows direction towards something i.e it points towards something. As a sign board points towards a location in real life, a ` head ` pointer points towards a **commit** object on which we are working. This pointer can moved to any commit with help of the command ` checkout `
```
git checkout <commit_hash>
```

### Branch
Yes!, the first thing that comes to our mind is a tree branch. We can imagine a inverted tree, wherein the top represents root, then trunk and finally  branches. Let's try to create analogy between an inverted tree and our project.<br/>
Our project originates from a simple idea i.e root/base. Initially everone is working on a basic main feature (for now) to show i.e trunk. As time passes, we need to incorporate multiple features in our project. Developers can work on UI, API's, authorization...etc. This can be done without impacting the main project by using branches. Anyone can create a branch and work on it.<br/>
* In simple words, a branch is a different path of work connected to the main project **without** directly impacting it.
  


#### Uses
* Parallel development : While development of a project, we need to work on different features on the same time. This is achieved through git branch. It offers different developers to work seperatly on their work without impacting others.
* Quick shifting to other branches offers flexibility to the developers.



#### Creation and working


To create a branch in git, we use `branch'
```
git branch <branch_name>
```

> * The HEAD still points to the previous branch, to make a branch and move the HEAD pointer to it use
>   ```
>   git checkout -b <branch_name>
>   ```

<br/>


>NOTE:<br/>
>* In the above commands, if the branch already exists, git will throw error

<br/>

To list the available branches, we can use `--list` or simply use 'branch` without any following words
```
git branch --list
git branch 
```

### Merging
As the word merge means "_combine or join together_", in branches it refers to the merging of two branches. This is uefull when work on a different branch is useful/completed and we want those in our current branch, we merge them. There are mainly two options for us to merge two branches
1. merge
2. rebase

#### 1.Merge
As mentioned above, this is the straightforward merge, wherein we combine the different files (and structure) into one. It can be achieved through ` merge ` command.
```
git merge <branch_name>
```
> While using merge command, the <branch_name> used,will be merged into our **current** working branch.
> It will preserve the commit history


#### 2. Rebase
Here the merge is carried out in such way as if both branches were developed in **linear** fashion. To rebase a branch **onto another** branch we use ` rebase `.
```
git rebase <branch_name>
```

> Though rebase merges two branches, but can alter the actual development and commit history.


#### conflict
While merging, the two branchs can modify same file differently. This leads to conflict (in both `merge` and `rebase`). To avoid this, git automatically modifies the confict file with pointers <br/>
```
<<<<<<< branch-1 (curretn change)
Hello world
========
Hello universe 
>>>>>>> branch-2 (incoming change)
```

To resolve the conflict, we have to manually modify the file according to our requirements and update it. Then continue with the process.
